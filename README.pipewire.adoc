= Using PipeWire under LADIOS
:keywords: catroof, ratbagd, libratbag, piper, dbus, d-bus, zbus, c, rust, python3, gtk, gnome, qt, kde
:docinfo: private-head,private-header
:doctype: article
:toc:

This document supplements https://wiki.gentoo.org/wiki/PipeWire#Replacing_JACK[the Gentoo wiki PipeWire as JACK documentation], in context of LADIOS.

== Setting profile of default audio device to "pro audio"

These instructions are based on https://linuxmusicians.com/viewtopic.php?p=171612#p171612[post by ironhouzi at LinuxMusicians forum on topic "Fedora Pipewire Low Latency Audio Configuration Reference Guide V.1.02"]

=== method 1, Use one line script
[source,bash]
pw-dump \
  $(wpctl inspect @DEFAULT_AUDIO_SINK@ | \
    grep "device.id" | \
    awk -F'"' '{gsub(/"/, "", $NF); print $2}') | \
  python3 -c 'import json, sys;'\
'dev, = (d for d in json.load(sys.stdin) if d["type"].endswith("Device"));'\
'profile, = (p for p in dev["info"]["params"]["EnumProfile"]'\
' if p["name"] == "pro-audio");'\
'print("wpctl set-profile", dev["id"], profile["index"])' | bash

=== method 2, Using wp-profile tool
[source,bash]
 curl -o wp-profile https://dl.ladish.org/wp-profile/wp-profile.py
 chmod +x wp-profile
 ./wp-profile

Example output:
++++
<pre class='stdout'>

Default audio device is hw:2 "UMC404HD 192k"

"UMC404HD 192k" profiles:

0: [off] "Off"
  priority: 0
1: [HiFi] "Default"
  priority: 9300
2: [Direct] "Direct UMC404HD 192k"
  priority: 2000
3: [pro-audio] "Pro Audio"
  priority: 1

To set pro-audio profile of default audio device run:

wpctl set-profile 64 3

</pre>
++++

== Adjust pipewire configuration for JACK
In apropriate pipewire configuration file,
set jack.self-connect mode to ignore-external
and optionally enable aliases:

[source,txt]
# global properties for all jack clients
jack.properties = {
     jack.self-connect-mode = ignore-external
     jack.fill-aliases = true
}

LADIOS specific configuration files for pipewire are:

 * per-user at `~/.config/pipewire/jack.conf.d/ladi.conf`
 * system-wide at `/etc/pipewire/pipewire.conf.d/ladi.conf`
